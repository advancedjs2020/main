import Vue from 'vue';

const helloWorld = Vue.component('button-counter', {
  data: function() {
    return {
      count: 0
    };
  },
  template: '<div><button v-on:click="count++">Счётчик кликов — {{ count }}</button></div>'
});

export default new helloWorld();


export const mount = (component, region) => {
  return component.$mount(region);
};

export const unmount = (component) => {
  return component.destroy();
};
