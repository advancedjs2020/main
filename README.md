# MediaPult project

## Prerequisites

To start the project on your local machine, you need to install following programs:

- **Node.js** LTS (Yarn is shipped with Node.js too)
- **Lerna** should be installed globally `yarn add lerna -g`
- (Optional) **NVM** (If you have old Node.js installed)
- (Optional) **Docker** and `docker-compose` (If you want to run synthetic testing)

## Installation

1. `git clone <...>`
2. `yarn` 
3. `git submodule init`
4. `git submodule update`
5. `lerna bootstrap`
6. `cd packages/bootstap`
7. `yarn start`

## How to add your own app

1. `cd apps`
2. `git checkout -b feature/new-app`
3. `git submodule add https://<your_repo>.git`
4. Add new entry to the `main/packages/bootstrap/webpack.config.dev.js`
5. Create new PR from feature/branch
